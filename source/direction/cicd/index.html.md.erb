---
layout: markdown_page
title: "Product Section Direction - CI/CD"
---

{:.no_toc}

- TOC
{:toc}

## Content Migration Note

The Verify, Package and Release stages [were recently added to the Ops section](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42825). You can still find some direction content from the former CI/CD section on this page. Over time [it will be migrating](https://gitlab.com/gitlab-com/Product/-/issues/912) to [Ops Section Direction](/direction/ops/) page. 

## CI/CD Overview

The CI/CD section focuses on the following stages of the [DevOps Lifecycle](/stages-devops-lifecycle/):

- Code build/verification ([Verify](/direction/cicd#verify))
- Packaging/distribution ([Package](/direction/cicd#package))
- Software delivery ([Release](/direction/cicd#release))

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](/images/cicd/gitlab_cicd_workflow_verify_package_release_v12_3.png "Pipeline Infographic")

CI/CD represents a large portion of the DevOps market with significant growth upside, and is also the gateway to the rest of our [Ops](/direction/ops) stages and features inside of GitLab. As of late 2019, the application release automation/CD and configuration management (mainly aligned to the Release stage) segment supported 984.7MM USD revenue in 2019, projected by 2022 to grow to 1617MM. The Continuous Integration, integrated quality/code review/static analysis/test automation, and environment management (mainly aligned to the Verify stage) represented an even larger 1785MM market in 2019, projected to grow to 2624MM. With a combined projected total of 4241MM by 2022, it's critical that we continue to lead here. __([market data, GitLab internal link](https://docs.google.com/spreadsheets/d/14j-C-9AzSRI2pEvX2zh42O34Y_EgDocEqffwv_KyJJ4/edit#gid=0))__

Team size and tracking against plan can be seen on our [hiring chart](https://about.gitlab.com/handbook/hiring/charts/cicd-section/). Maturity for each of the stages can be found on our [maturity page](/direction/maturity).

## Competitive Space and Positioning

CI/CD solutions have continued to rapidly innovate. Kubernetes itself, a massive driver of industry change, just turned five years old in 2019. Docker, now considered a mature and foundational technology, was only released in 2013. We do not expect this pace to slow down, so it's important we balance supporting the current technology winners (Kubernetes, AWS, Azure, and Google Cloud) with best-of-breed, built-in solutions, while at the same time avoiding over-investment on any single technology solution that could become legacy. Our [Multi-Platform Support theme](/direction/cicd/#multi-platform-support) highlights the technologies we're monitoring and investing in.

There are dangers to falling out of balance; Pivotal resisted embracing Kubernetes early and has [suffered for it](https://fortune.com/2019/07/29/ford-pivotal-write-down/). At the same time, technologies like Jenkins that deeply embraced what are now considered legacy paradigms are enjoying the long tail of relevancy, but are having to [scramble to remain relevant for the future](https://jenkins.io/blog/2018/08/31/shifting-gears/).

In summary, this space is highly competitive with large competitors moving towards a single application, and smaller competitors innovating nimbly on features. GitHub in particular has a strong product positioning and adoption, and is our rival that we need to monitor most closely. Spinnaker (and to a certain extent Drone) are doing very well innovating on CD technology. Spinnaker in particular is growing fast and enabling [collaborative CI/CD workflows using automation](https://opensource.com/article/19/8/why-spinnaker-matters-cicd) similar to what we want to provide; we need to mature our CD offering before they become dominant by ensuring there is no reason to prefer Spinnaker over our own solutions. At the same time, some competitors such as Jenkins are falling behind in relevance. We have an opportunity to replace these kinds of tools with our own more modern solution, and we need to ensure that we make this as simple as possible.

For Spinnaker you can read our strategy and competitive analysis on the [direction page for Continuous Delivery](/direction/release/continuous_delivery/#spinnaker). Against Jenkins, the equivalent content is avilable on the [direction page for Continuous Integration](/direction/verify/continuous_integration/#cloudbees-jenkinscodeship)

## Strategy

Our strategy for CI/CD right now is all about enabling easy-to-discover workflows that support doing powerful, complex CI/CD actions with a minimum of manual configuration. We want to take advantage of our single application so that, while each team may have their own views or dashboards in the product that support their day to day, the information about what is deployed where is available everywhere and to everyone, embedded naturally into the product where it's relevant. For example, a person thinking about upcoming releases may interact mostly with an environment-based overview that helps them see upcoming changes as they flow environments, but that same information exists everywhere it is relevant: 

- Testers looking at an individual issue can see which environment(s) that issue has been deployed to
- Developers reviewing a merge request have the Review App at their fingertips
- Feature flags link back to the issues and merge requests that introduced them for context
- Upcoming releases have burndown charts right in the overview
- Evidence collection for auditors happens automatically throughout the pipeline, with nothing special to set up

The end result is that even complex delivery flows become part of everyone's primary way of working. There isn't a context shift (or even worse, a switch into a different tool) needed to start thinking about delivery - that information is there, in context, from your first commit. The centerpiece of this strategy is our [Get Things Done Easily theme](/direction/cicd/#get-things-done-easily).

## Plan & Themes

Given our vision and strategy, the top priorities in our plan are:

- Continue to double down on the strengths in our section, effectively competing with the companies in our market. Our strengths and opportunities are enumerated in the themes below, and this would also include making it easy to move to GitLab CI/CD from other products.
- Get to product leadership in CD through two main paths: credible competition with products like Spinnaker, and to build the first complete Progressive Delivery solution.
- Build flows in the product that help connect the dots between SCM, CI, Packaging, and CD. We will work on our user experience funnel, figuring out where people are dropping off and losing the thread that results in them not getting the most out of GitLab. We know for example we are undeserving non-k8s container deployments.
