---
layout: job_family_page
title: "UX Research Management"
---

## UX Research Management Roles at GitLab

Managers in the UX Research department at GitLab see the team as their product. While they are credible as researchers and know the details of what UX Researchers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX Research commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### UX Research Manager

The User Experience (UX) Research Manager reports to the Senior Manager or Director of UX Research, and UX Researchers report to the UX Research Manager.

#### Responsibilities

* Lead and mentor UX Researchers to support their career development, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Share research expertise and knowledge with direct reports and cross-functional partners, including helping team members select the right methodology for their research projects.
* Encourage the wider organization to actively participate in UX Research.
* Help operationalize UX Research to reduce inefficiencies, and make UX Research scalable through repeatable processes, ready-to-apply methods, and templates.
* Help identify and select the tools needed to mature our UX Research practices.
* Evangelize GitLab UX Research efforts outside of the organization with the goal of sharing the best of what our team produces with others who can benefit. 
* Regularly meet with Product Management Directors to understand strategy, discover blockers, manage quality, and help ensure UX prioritization.
* Review research deliverables that your team creates, and provide feedback to ensure high-quality output.
* Partner with colleagues in Product Design, Product Management, Product Marketing, Engineering, Sales, and Support. 
* Build relationships with other managers across disciplines, and act as a liaison with teams throughout the wider organization that have ongoing research needs.
* Ensure that UX Researchers have clearly prioritized deliverables for every milestone.
* Hold regular 1:1s with team members.
* Hire a world-class team of UX Researchers.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 5 years of experience in hands-on research.
* A minimum of 3 years of experience managing UX Researchers or large research teams.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of driving change through UX Research.
* Passion for the field of UX Research.
* Able to use GitLab to plan/manage work and update the company handbook.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Senior UX Research Manager

The Senior User Experience (UX) Research Manager reports to the Director of UX, and UX Research Managers report to the Senior UX Research Manager.

#### Responsibilities
* Create an open and collaborative culture based on trust in the UX Research team.
* Develop an overarching UX research strategy for GitLab, and work with the UX Research team and cross-functional partners to execute that strategy.
* Mentor UX Research Managers in their career growth, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Help scale UX Research by defining a plan to provide research training for Product Managers and Product Designers, and coordinate the UX Research team to create and maintain training materials.
* Actively advocate for UX Research throughout the company, so that the company is aware of what UX Research does, how they do it, and what insights they have delivered.
* Stay informed of industry best practices, and help the team to evolve our research practice accordingly. 
* Identify ways to more efficiently and effectively conduct remote user research, and share those learnings with the wider community.
* Work with Product Operations to ensure UX research is included appropriately in the [Product Development Flow](/handbook/product-development-flow/).
* Define and manage [Performance Indicators](/handbook/engineering/ux/performance-indicators/) for the UX Research team.
* Create and manage [OKRs](/company/okrs/) for the UX Research team with guidance from UX Leadership.
* Align the way that UX Researchers work with how their cross-functional partners in Product Managment, Product Design, and Engineering prioritize, track, and manage work.
* Conduct regular [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with their direct reports and quarterly [skip-level meetings](https://about.gitlab.com/handbook/leadership/skip-levels/) with the reports of their direct reports.
* Provide clear and direct feedback to the people they manage, their own manager, and cross-functional partners when needed.
* Hire a world-class team of UX Researchers.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 8 years of experience in hands-on research.
* A minimum of 2 years of experience managing UX Research Managers.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of leading a team that drives change through UX Research. 
* Experience advocating for UX Research and broadly communicating about research insights to stakeholders and partners.
* Passion for the field of UX Research.
* Able to use GitLab to plan/manage work and update the company handbook.
* Ideally, has experience leading a remote UX research team that conducts research fully remote, too.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Director of UX Research

The Director of UX Research reports to the Director of UX, and UX Research Managers report to the Director of UX Research.

#### Responsibilities
* Create an open and collaborative culture based on trust in the UX Research team.
* Set an ambitious vision for UX Research that other companies use as a benchmark for their own research initiatives.
* Align UX Research to business goals and company revenue and clearly communicate the resulting impact.
* Develop an overarching UX research strategy for GitLab, and work with the UX Research team and cross-functional partners to execute that strategy.
* Mentor UX Research Managers in their career growth, including making sure they each have an [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources).
* Help scale UX Research by defining a plan to provide research training for Product Managers and Product Designers, and coordinate the UX Research team to create and maintain training materials.
* Actively advocate for UX Research throughout the company, so that the company is aware of what UX Research does, how they do it, and what insights they have delivered.
* Stay informed of industry best practices, and help the team to evolve our research practice accordingly. 
* Identify ways to more efficiently and effectively conduct remote user research, and share those learnings with the wider community.
* Work with Product Operations to ensure UXR consideration in the [Product Development Flow](/handbook/product-development-flow/).
* Define and manage [Performance Indicators](/handbook/engineering/ux/performance-indicators/) for the UX Research team.
* Independently create and manage [OKRs](/company/okrs/) for the UX Research team.
* Align the way that UX Researchers work with how their cross-functional partners in Product Managment, Product Design, and Engineering prioritize, track, and manage work.
* Conduct regular [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with their direct reports and quarterly [skip-level meetings](https://about.gitlab.com/handbook/leadership/skip-levels/) with the reports of their direct reports.
* Provide clear and direct feedback to the people they manage, their own manager, and cross-functional partners when needed.
* Hire a world-class team of UX Researchers.
* Manage the UX Research budget with oversight from UX leadership.

#### Requirements

* Expert-level knowledge in the field of UX Research with at least 10 years of experience in hands-on research.
* A minimum of 4 years of experience managing UX Research Managers.
* Outstanding communicator both verbally and in writing.
* Strong collaboration skills.
* A proven record of leading a team that drives change through UX Research. 
* Experience advocating for UX Research and broadly communicating about research insights to stakeholders and partners.
* Passion for the field of UX Research.
* Able to use GitLab to plan/manage work and update the company handbook.
* Ideally, has experience leading a remote UX research team that conducts research fully remote, too.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Performance indicators
* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with a UX Researcher (report)
- Interview with a UX Director (manager)
- Interview with a UX Manager (peer)
- Interview with VP of Engineering
